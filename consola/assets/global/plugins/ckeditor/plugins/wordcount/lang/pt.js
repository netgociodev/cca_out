CKEDITOR.plugins.setLang('wordcount', 'pt', {
    WordCount: 'Palavras:',
    CharCount: 'Caracteres:',
    CharCountWithHTML: 'Carateres (incluindo HTML):',
    Paragraphs: 'Par&aacute;grafos:',
    pasteWarning: 'O conte&uacute;do n&atilde;o pode ser colado porque ultrapassa o limite permitido',
    Selected: 'Selecionado: ',
    title: 'Estat&iacute;sticas'
});
