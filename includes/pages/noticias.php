<?php include_once('pages_head.php');

geraSessions('noticias');
$row_rsNoticias = $GLOBALS['divs_noticias'];

$query_rsImagem = "SELECT noticias FROM imagens_topo";
$rsImagem = DB::getInstance()->query($query_rsImagem);
$row_rsImagem = $rsImagem->fetch(PDO::FETCH_ASSOC);
$totalRows_rsImagem = $rsImagem->rowCount();
DB::close();

$menu_sel="noticias";
?>

<main class="page-load noticias">
    <?php
    $img = "elem/topo.jpg";
    if($row_rsImagem['noticias'] && file_exists(ROOTPATH.'imgs/imagens_topo/'.$row_rsImagem['noticias'])){
       $img = "imagens_topo/".$row_rsImagem['noticias']; 
    }
    ?>
    <div class="div_100 banners banner_contactos has_bg has_mask lazy" data-src="<?php echo $img; ?>" style="margin-bottom: 0;">
        <?php echo getFill('imagens_topo'); ?>    
        <div class="div_absolute" style="padding:0">    
            <div class="row align-middle" style="height: 100%;">
                <div class="column small-12">
                    <div class="banner_content text-center" style="max-width: unset;">
                        <h1 class="titulos show" style="color: white"><?php echo $Recursos->Resources["noticias"]; ?></h1>
                    </div>
                </div>
            </div> 
        </div> 
    </div>

	<nav class="breadcrumbs_cont" aria-label="You are here:" role="navigation">
        <div class="row">
            <div class="column">
                <ul class="breadcrumbs">
                	<li class="disabled"><span><?php echo $Recursos->Resources["bread_tit"]; ?></span></li>
                    <li><a href="<?php echo get_meta_link(1); ?>" data-ajaxurl="<?php echo ROOTPATH_HTTP; ?>includes/pages/index.php" data-remote="true"><?php echo $Recursos->Resources["home"]; ?></a></li>
                    <li>
                         <span><?php echo $Recursos->Resources["noticias"]; ?></span>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    
    <div class="row">
        <div class="column">
        <?php if(!empty($row_rsNoticias)){ ?>
            <div class="noticias_cont text-left">                           
                <?php foreach($row_rsNoticias as $noticias){
                    if($noticias['info']){
                        $noticias = $noticias['info'];
                    }

                //$img = ROOTPATH_HTTP."imgs/elem/geral.svg";
                $img = "elem/geral.svg";
                if($noticias['imagem1'] && file_exists(ROOTPATH.'imgs/noticias/'.$noticias['imagem1'])){
                    //$img = ROOTPATH_HTTP."imgs/noticias/".$noticias['imagem1'];
                    $img = "noticias/".$noticias['imagem1'];
                }
                
                $data = explode(" ", $noticias['data']);
                ?><!--
                --><article class="noticias_divs" id="noticia<?php echo $noticias['id']; ?>">
                    <figure>
                        <picture class="img has_bg has_mask icon-mais lazy" data-src="<?php echo $img; ?>">
                            <?php echo getFill('noticias', 2); ?> 
                        </picture>  
                        <figcaption class="info text-left">
                            <h5 class="list_tit"><?php echo $noticias['nome']; ?></h5>
                            <div class="textos"><?php echo str_text($noticias['resumo'], 300); ?></div>
                            <button class="button"><?php echo $Recursos->Resources["saiba_mais"];?></button>
                            <a href="<?php echo $noticias['url']; ?>" class="linker" data-ajaxurl="<?php echo ROOTPATH_HTTP; ?>includes/pages/noticias-detalhe.php" data-ajaxTax="<?php echo $noticias['id']; ?>" data-remote="true" data-pagetrans="noticias-detail" data-detail="1"></a>
                        </figcaption>
                    </figure>
                </article><!--
                --><?php } ?>
            </div>
        
        <?php }else{ ?>
            <h6 class="sem_prods"><?php echo $Recursos->Resources["sem_produtos"]; ?></h6>
        <?php } ?>
        </div>
    </div>          
</main>


<?php include_once('pages_footer.php'); ?>