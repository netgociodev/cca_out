<?php include_once('pages_head.php');

geraSessions('banners');
geraSessions('homepage');
geraSessions('destaques');
geraSessions('novidades');
geraSessions('promocoes');

$menu_sel = "home";
?>
<main class="page-load homepage">
	<section class="div_100" id="banners">
		<?php include_once(ROOTPATH.'includes/index_banners.php'); ?>
	</section>

	<?php if(!empty($GLOBALS['divs_destaques'])) { ?>
		<section class="div_100" id="destaques">
			<div class="row medium-collapse">	
				<div class="column medium-offset-3 text-center medium-text-left">
					<h3 class="titulos uppercase bold"><?php echo $Recursos->Resources["destaques"]; ?></h3>
					<div class="outer">
						<div class="div_100">
							<div class="slick-of3 has_dots">
								<?php foreach($GLOBALS['divs_destaques'] as $destaques) { ?>
									<?php echo $class_produtos->divsProduto($destaques, ''); ?>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</section>
	<?php } ?>

	<?php if(!empty($GLOBALS['divs_novidades']) || !empty($GLOBALS['divs_promocoes'])) { ?>
		<?php 
		$style = "background-color:#F3F2F3;";
		if($GLOBALS['divs_homepage']['imagem1'] && file_exists(ROOTPATH.'imgs/homepage/'.$GLOBALS['divs_homepage']['imagem1'])) { 
			$style = "background-image:url('".ROOTPATH_HTTP."imgs/homepage/".$GLOBALS['divs_homepage']['imagem1']."');";
		}
		?>
		<section class="div_100" id="produtos">
			<picture class="div_100 has_bg fixed" style="<?php echo $style; ?>">
				<?php echo getFill('homepage'); ?>
			</picture>
			<div class="row medium-collapse">	
				<div class="column medium-offset-3">
					<div class="div_100 tabs">
						<?php if(!empty($GLOBALS['divs_novidades'])) { ?><a class="subtitulos uppercase text-center" href="javascript:;"><?php echo $Recursos->Resources["novidades"]; ?></a><?php } ?><!-- 
						--><?php if(!empty($GLOBALS['divs_promocoes'])) { ?><a class="subtitulos uppercase text-center" href="javascript:;"><?php echo $Recursos->Resources["promocoes"]; ?></a><?php } ?>
					</div>
					<div class="div_100 tabs-content">
						<?php if(!empty($GLOBALS['divs_novidades'])) { ?>
							<div class="div_100 tab">
								<div class="slick-of3 has_dots">
									<?php foreach($GLOBALS['divs_novidades'] as $novidades) { ?>
										<?php echo $class_produtos->divsProduto($novidades, ''); ?>
									<?php } ?>
								</div>
							</div>
						<?php } ?>
						<?php if(!empty($GLOBALS['divs_promocoes'])) { ?>
							<div class="div_100 tab">
								<div class="slick-of3 has_dots">
									<?php foreach($GLOBALS['divs_promocoes'] as $promocoes) { ?>
										<?php echo $class_produtos->divsProduto($promocoes, ''); ?>
									<?php } ?>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>	
			</div>
		</section>
	<?php } ?>

	<?php if($GLOBALS['divs_homepage']['imagem2'] && file_exists(ROOTPATH.'imgs/homepage/'.$GLOBALS['divs_homepage']['imagem2'])) { ?>
		<section class="div_100 banners" id="portfolio">
			<div class="row medium-collapse">	
				<div class="column medium-offset-3">
					<div class="div_100 has_bg has_mask clipped-left lazy" data-src="homepage/<?php echo $GLOBALS['divs_homepage']['imagem2']; ?>">
						<?php echo getFill('homepage', 2); ?>
						<div class="banner_cont">
							<div class="row collapse full align-middle" style="height: 100%;">
								<div class="column small-12">                                   
									<div class="banner_content">
										<?php if($GLOBALS['divs_homepage']['titulo2']) { ?>
											<h2 class="titulos show"><?php echo $GLOBALS['divs_homepage']['titulo2']; ?></h2>
										<?php } ?>
										<?php echo text_link($GLOBALS['divs_homepage']['link2'], $GLOBALS['divs_homepage']['target2'], $GLOBALS['divs_homepage']['texto_link2'], "button show"); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</section>
	<?php } ?>

</main>

<?php include_once('pages_footer.php'); ?>